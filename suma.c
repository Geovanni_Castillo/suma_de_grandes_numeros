#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>

int chartoint(char a){
    int i;

    for (i = 48; i<=57; i++){
        if (toascii(i) == a) return i-48;
    }

    return 0;

}

typedef unsigned int uint;

void strrev(char* str){

    char tmp;

    for(uint i = 0, j = strlen(str)-1; i < j; j--, i++) {
        tmp = str[i]; str[i] = str[j]; str[j] = tmp;
    }

}

void main(){

    int i, j, m, cmax, sum;

    char num1[80];
    char num2[80];

    int rs[80];
    int c1, c2;

    printf("\t---Suma de Números Grandes---\n");

    printf("Primer Número: ");
    scanf("%s", num1);

    printf("Segundo Número: ");
    scanf("%s", num2);

    c1 = strlen(num1);
    c2 = strlen(num2);


    strrev(num1);
    strrev(num2);

    cmax = c1;
    if(c1<c2){
        cmax = c2;
    }

    m=0;
    for(i=0; i< cmax; i++){

        if(c1==c2 || (i < c1 && i < c2)){
            sum = m+chartoint(num1[i])+chartoint(num2[i]);

        }else if(i >=c1){
            sum = m+chartoint(num2[i]);
            
        }else if(i >=c2){
            sum = m+chartoint(num1[i]);

        }
        
        rs[i] = sum%10;
        m = sum/10;

    }

    if(m){
        rs[i]=m;
        i++;
    }

    printf("\nResultado: ");
    for(j=0; j < i; j++){
        printf("%d", rs[i-j-1]);
    }

    printf("\n");
    
}
